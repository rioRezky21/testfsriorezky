package repository

import (
	"fmt"
	"testfs/driver"
	"testfs/models"

	"golang.org/x/crypto/bcrypt"
)

type ResponseModel struct {
	Code    int    `json:"code" validate:"required"`
	Message string `json:"message" validate:"required"`
}

//Read By Page
func ReadByPage(limit int, page int) []models.UserModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.UserModel
	var pageNumber = (page - 1) * limit

	items, err := db.Query(`SELECT * FROM users ORDER BY id LIMIT ? OFFSET ?`, limit, pageNumber)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T", items)

	for items.Next() {
		var each = models.UserModel{}
		var err = items.Scan(&each.Id, &each.Username, &each.Password, &each.Name)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)
	}

	if err = items.Err(); err != nil {
		fmt.Println(err.Error())
		return nil
	}

	return result
}

//Read By Id
func ReadById(userId int) []models.UserModel {
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	defer db.Close()

	var result []models.UserModel

	items, err := db.Query(`SELECT * FROM users WHERE id = ?`, userId)

	if err != nil {
		fmt.Println(err.Error())
		return nil
	}

	fmt.Printf("%T", items)

	for items.Next() {
		var each = models.UserModel{}
		var err = items.Scan(&each.Id, &each.Username, &each.Password, &each.Name)

		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		result = append(result, each)
	}

	if err = items.Err(); err != nil {
		fmt.Println(err.Error())
		return nil
	}

	return result
}

// Create
func CreateUser(L *models.UserModel) *ResponseModel {
	Res := &ResponseModel{500, "internal sever error"}
	bytes, err := bcrypt.GenerateFromPassword([]byte(L.Password), 14)
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}
	defer db.Close()

	username := L.Username
	password := L.Password
	name := L.Name
	hashPassword := string(bytes)

	if len(username) > 3 {
		if len(password) > 7 {
			if len(name) > 3 {
				_, err = db.Exec(`INSERT INTO users (username, password, name) VALUES (?, ?, ?)`, L.Username, hashPassword, L.Name)
			} else {
				Res = &ResponseModel{500, "name is less than 3 character"}
				return Res
			}
		} else {
			Res = &ResponseModel{500, "password is less than 3 character"}
			return Res
		}

	} else {
		Res = &ResponseModel{500, "username is less than 3 character"}
		return Res
	}

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "failed to save data because username has been used, try another username"}
		return Res
	}

	fmt.Println("insert Success!")
	Res = &ResponseModel{200, "data has been saved"}

	return Res

}

// Delete
func DeleteUser(userId int) *ResponseModel {
	Res := &ResponseModel{500, "internal Server Error"}
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	_, err = db.Exec("delete from users where id = ?", userId)
	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "failed to delete data"}
		return Res
	}

	fmt.Println("Delete Success!")
	Res = &ResponseModel{200, "data has been deleted"}
	return Res
}

// Update
func UpdateUser(L *models.UserModel, userId int) *ResponseModel {
	Res := &ResponseModel{500, "internal server error"}
	bytes, err := bcrypt.GenerateFromPassword([]byte(L.Password), 14)
	db, err := driver.ConnectDB()

	if err != nil {
		fmt.Println(err.Error())
		return Res
	}

	defer db.Close()

	username := L.Username
	password := L.Password
	name := L.Name
	hashPassword := string(bytes)

	if len(username) > 3 {
		if len(password) > 7 {
			if len(name) > 3 {
				_, err = db.Exec("update users set username = ?, password = ?, name = ? where id= ?", L.Username, hashPassword, L.Name, userId)
			} else {
				Res = &ResponseModel{500, "name is less than 3 character"}
				return Res
			}
		} else {
			Res = &ResponseModel{500, "password is less than 3 character"}
			return Res
		}

	} else {
		Res = &ResponseModel{500, "username is less than 3 character"}
		return Res
	}

	if err != nil {
		fmt.Println(err.Error())
		Res = &ResponseModel{400, "failed to update data because username has been used, try another username"}
		return Res
	}

	fmt.Println("Update Success")
	Res = &ResponseModel{200, "data has been updated"}
	return Res

}
