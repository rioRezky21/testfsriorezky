package routes

import (
	"testfs/service"

	"github.com/labstack/echo"
)

func EndPoint() {
	e := echo.New()
	e.GET("/user/:userId", service.ReadById)
	e.GET("/user/:limit/:page", service.ReadByPage)
	e.POST("/user", service.CreateUser)
	e.PATCH("/user/:userId", service.UpdateUser)
	e.DELETE("/user/:userId", service.DeleteUser)

	e.Logger.Fatal(e.Start(":1323"))
}
