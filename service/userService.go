package service

import (
	"net/http"
	"strconv"
	"testfs/models"
	"testfs/repository"

	"github.com/labstack/echo"
)

type ResponseModel struct {
	Code    int    `json:"code" validate:"required"`
	Message string `json:"message" validate:"required"`
}

func ReadByPage(c echo.Context) error {
	limit := c.Param("limit")
	page := c.Param("page")
	pageLimit, _ := strconv.Atoi(limit)
	pageNumber, _ := strconv.Atoi(page)
	result := repository.ReadByPage(pageLimit, pageNumber)
	return c.JSON(http.StatusOK, result)
}

func ReadById(c echo.Context) error {
	id := c.Param("userId")
	data, _ := strconv.Atoi(id)
	result := repository.ReadById(data)
	return c.JSON(http.StatusOK, result)
}

func CreateUser(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	Body := new(models.UserModel)
	if err := c.Bind(Body); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.CreateUser(Body))
	return c.JSON(http.StatusOK, Res)
}

func DeleteUser(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.Param("userId")
	data, _ := strconv.Atoi(id)
	Res = (*ResponseModel)(repository.DeleteUser(data))
	return c.JSON(http.StatusOK, Res)
}

func UpdateUser(c echo.Context) error {
	Res := &ResponseModel{400, "Bad Request"}
	id := c.Param("userId")
	data, _ := strconv.Atoi(id)
	Body := new(models.UserModel)
	if err := c.Bind(Body); err != nil {
		return nil
	}
	Res = (*ResponseModel)(repository.UpdateUser(Body, data))
	return c.JSON(http.StatusOK, Res)
}
